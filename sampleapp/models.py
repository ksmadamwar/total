from django.db import models
from django.conf import settings
from testproject.settings import db
from bson.json_util import dumps
import json

# Create your models here.

def sap_raw(request):
    
	# here you can pass collection name inside the list for this database.
	# surface care is the collection name(table name)
    table_name  = db['Sap_Raw']     
    
    data= json.loads(dumps(table_name.find({})))
    return data
	

