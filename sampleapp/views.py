from django.http import HttpResponse
from django.shortcuts import render
from .models import sap_raw
from django.http import JsonResponse
from django.conf import settings
from testproject.settings import db
from bson.json_util import dumps
import json


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def sap_raw(request):
	# call channels function from models and get the result 
	table_name  = db['Sap_Raw']     
	print (table_name.count())
	data= json.loads(dumps(table_name.find({})))
	return JsonResponse(data, safe=False)


