from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('data', views.sap_raw, name='sap_raw'),
]